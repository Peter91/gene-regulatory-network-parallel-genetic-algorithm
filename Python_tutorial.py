
# coding: utf-8

### A continuación vamos a ver algunas de las operaciones que podemos hacer en python, dada su cantidad de funciones vamos a ver las más comunes a la hora de usar python y algunas de especial interés para el desarrollo del trabajo.   Operaciones con tipos númericos, int,long y float:

# In[4]:

"""Podemos definir una variable con el constructor vacío"""
n=int() 
f=float()
print n,f


# In[5]:

n=100
f=9.6
print n,f


### Operaciones con listas

# In[2]:

"""podemos definar una variable con el constructor vacío"""
l=list()
"""O directamente le damos valores a la lista"""
l=[1,2,3,4,5,6,7,8]
print l


"""Para acceder a un elemento de la lista podemos llamarlo por su índice"""
l[0] # -> 1
l[0] = 3 #Modificamos el valor de índice 0 a 3, l[0] -> 3
print l[:2]
print l[2:6]
print l[2:]

"""Métodos"""
l.append(3) #Inserta al final de la lista el elemento 3
print l
l.extend([1,2,3]) #Añade la lista que se pasa como parámetro al final
print l
print l.index(2) #Devuelve el índice del primer elemento de la lista que coincida con el del parámetro
l.reverse() #Da la vuelta a la lista
print l 
l.sort() #Ordena la lista según sus elemento
print l 


# In[1]:

"""Iterar sobre listas"""
l=[1,2,3,4]
for x in l:
    print x


### Operaciones con conjuntos 

# In[4]:

"""podemos definar una variable con el constructor vacío"""
s=set()
"""O set(iterable)"""
s=set([1,2,3])
print s
s.add((1,2))#añadimos el elementos (1,2)
s.add((4,5))#añadimos el elementos (4,5)
print s
s.remove(2) #eliminamos el elemento 2
print s


# In[5]:

print len(s) #Número de elementos del conjunto/ cardinalidad
print (1,2) in s #Pregunta si el elemento x está en s
print (4,5) not in s #Pregunta si el elemento x no está en s
r=set([1,2,(1,2),9,10,(9,8)])
print s.union(r) #Unión de s con t
print s.intersection(r)#Intersección de s con t
print s.difference(r) #Diferencia de s y t
v=s.copy()#Crea un copia del objeto conjunto s en la variable v.
print v


### Tuplas

# In[11]:

"""podemos definar una variable con el constructor vacío"""
t=tuple()
"""O directamente la damos valores"""
t=(8,4,5,6,[1,3],'b')
print t


# In[12]:

"""Las tuplas no tiene método append para añadir elementos
o método extend para extender la tupla,
son un tipo de objet inmutable que no podemos modificar su contenido,
tan sólo ver si un elemento está contenido, acceder por su indice e iterar sobre ellas"""
print t[1] #Elemento del indice 1
print [1,3] in t
t[1]=5


# In[13]:

"""Iterar sobre tuplas"""
for i in t:
    print i


### Diccionarios

# In[6]:

"""Es la estructura clave-valor, donde las claves son únicas, es decir, no puede haber claves repetidas
y los valores pueden ser 1 o varios valores"""

d=dict()
d={'jack': 4098, 'sape': 4139}
print d
f=dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
print f


# In[7]:

d['jack'] #accedemos por la clave
'sape' in f or d.has_key('sape')
f['hola']=320 #añadimos un nuevo elemento de clave 'hola' valor 320
print f
print f.items() #Devuelve una lista con tuplas (clave,valor)
print f.values() #Devuelve una lista con los valores


# In[14]:

"""Iteramos sobre un diccionario"""
for i in d: #Itera sobre las claves
    print i,d[i]


### TEcnias iterativvas

# In[11]:

l=[1,2,3,4,5]
"""La funcion range(x), nos devuelve una lista de valores de tamaño x, desde 0 hasta x-1 o range(x,y) entre x e y-1"""
d=range(1,6)
print l,d     


# In[12]:

l=list()
for x in range(10):
    l.append(x)
print l


# In[22]:

l=range(1,10)
"""La funcion enumerate nos devuelve el elemento y el índice del elemento"""
for i,e in enumerate(l):
    print i,e


# In[17]:

l=range(1,10) #[ 1, 2, 3, 4, 5, 6, 7, 8, 9]
s=range(11,20) #[11, 12, 13, 14, 15, 16, 17, 18, 19]
r=range(21,30)
"""La funcion zip(s,l) nos devuelve elementos a pares,ternas... del mismo indice comenzando por le indice 1"""
for i,j in zip(l,s):
    print i,j


# In[18]:

for i,j,x in zip(l,s,r):
    print i,j,x


### Listas por comprensión 

# In[3]:

#Obtenemos una lista del 0 al 9
l=[x for x in range(10)]
print l


# In[4]:

#Filtramos por los elementos partes del 0 al 9
l=[x for x in range(10) if x%2==0]
print l


# In[5]:

#Multiplicamos por 2 los elementos pares del 0 al 9
l=[x*2 for x in range(10) if x%2]
print l


# In[6]:

x=[3,6,8,2,5]
y=[1,4,6,5,1]
#Obtenemos todas la combinaciones de dos elementos de las listas x e y
l=[(i,j) for i in x for j in y  ]
print l


# In[7]:

x=[3,6,8,2,5]
y=[1,4,6,5,1]
#Obtenemos pares de números por indice de las listas x e y
l=[(i,j) for i,j in zip(x,y)]
print l


# In[10]:

matrix=[[3,6,8,2,5],[1,4,6,5,1],[1,4,5,6,7]]
l=[[row[k] for row in matrix] for k in range(5)]
print l


# In[ ]:



