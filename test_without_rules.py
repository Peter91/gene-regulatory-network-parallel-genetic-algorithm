import random
from deap import tools
from deap import base, creator

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
"""Here define the individual data structure,
    we want a list number
    list
"""
creator.create("Individual", list, fitness=creator.FitnessMax)

NNUM= 3
MAXIN= 3
IND_SIZE = NNUM*MAXIN

toolbox = base.Toolbox()
# Attribute generator
"""Here define the individual elements,
    we want individuals with numbers between -1 and 1
    random.randint(-1, 1)
"""
toolbox.register("attribute", random.randint, -1, 1)

"""Here register the individual
    it mean, that an individual has repeat number definde in attribute and into a list
    and length list is n=IND_SIZE
    """
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attribute, n=IND_SIZE)

"""Here register the population
    it mean, that a population has repeat number individuals, data strucutre is a list
    """
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

def eval_indv(chromosome, final_state,lamb):
    """B is the graph
        links is the neiborhs

        lambda*dist[B,B_final]/max(dist[B,B_final]) +
        lambda-1*dist[links,links_final]/max(dist[links,links_final])"""   
    #2nd Links distance: dist[links,links_final]
    
    link_c=0
    for i,j in zip(chromosome,final_state):
        if i!=j:
            link_c+=1    
    
    """lambda-1*dist[links,links_final]/max(dist[links,links_final])"""
    
    link_count= ((1-lamb)*link_c)/len(chromosome)    
    total=link_count
    return total,

def cx_indv(ind1, ind2, nd_number, max_input):
    """To do crosver:
    1st we need 1 pivot to links parts and exchange
    2nd We check if new chrom. have +- 10 percent of conectivity than parents
    3th If 2nd True, we obtain new pivot to exchange rules and return new chrom.
        else return original chrom.
    """
    p= nd_number*max_input              
    
    #1st Links part
    links1=list(ind1)
    links2=list(ind2)
    pnt= random.randrange(len(links1))

    kid1=list(links1[:pnt]+links2[pnt:])
    kid2=list(links2[:pnt]+links1[pnt:])
    
    #2nd check conectivity
    """First we find parents conectivity"""    
    ind1_conect=0
    ind2_conect=0    
    for x,y in zip(ind1,ind2):
        if x!=0:
            ind1_conect+=1
        if y!=0:
            ind2_conect+=1
    """Second we find kid conectivity"""
    kid1_conect=0
    kid2_conect=0    
    for x,y in zip(kid1,kid2):
        if x!=0:
            kid1_conect+=1
        if y!=0:
            kid2_conect+=1
    """Third we compare """
    mean_parent_conect=(ind1_conect+ind2_conect)/2
    ten_pcnt= mean_parent_conect*0.1
    mean_kid_conect=(kid1_conect+kid2_conect)/2

    if( (mean_kid_conect <= (mean_parent_conect+ten_pcnt)) or
        (mean_kid_conect >= (mean_parent_conect-ten_pcnt)) ):
        ind1=kid1
        ind2=kid2
    
    return ind1,ind2

def mut_indv(individual,links_prob_mut,nd_number, max_input):
    """1� Miramos la probabilidad de nodos(0.0001) y reglas(0.5) por 
        cada nodo y regla. Si es igual o inferior muta entre 1/2 de posibilidad para los
        links y 1/3 para las reglas"""
    
    p=nd_number*max_input
    links_val= set([1,0,-1])
    for x in individual:
        p1=random.random()
        l=set([x])
        if (p1 <= links_prob_mut):
            n_link=random.choice(list(links_val-l))
            xi=individual.index(x)
            individual[xi]=n_link
       

    return individual

"""Here we register last define function in toolbox deap framework
    to use it them"""
toolbox.register("evaluate", eval_indv)
toolbox.register("crosver", cx_indv)
toolbox.register("mutate", mut_indv)

toolbox.register("select", tools.selTournament, tournsize=3)


def main(nodes_number,max_input_nodes, mut_prob_links, lambda_fitness,graph_init_state,graph_final_state):
    pop = toolbox.population(n=30)
    NGEN = 40
    """
    Variables globales:
            NNUM->número de nodos
            MAXIN->maximo número de entradas
            MUTPBL->probabilidad de mutación en links
            LF-> parámetro lamda de la función fitness
            IS-> estado inicial del grafo.
            FS->estado final del grafo.
    """
    NNUM= nodes_number
    MAXIN=max_input_nodes
    MUTPBL=mut_prob_links
    LF=lambda_fitness
    IS=graph_init_state
    FS=graph_final_state
    
    # Evaluate the entire population
    fitnesses = list()
    
    fitnesses= [toolbox.evaluate(i,FS,LF) for i in pop]
    
    print "fitnesses %i" , fitnesses
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit

    for g in range(NGEN):
        print("-- Generation %i --" % g)
        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop))
        # Clone the selected individuals
        offspring = map(toolbox.clone, offspring)

        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            
            toolbox.crosver(child1, child2, NNUM,MAXIN)
            del child1.fitness.values
            del child2.fitness.values

        for mutant in offspring:

            toolbox.mutate(mutant,MUTPBL,NNUM,MAXIN)
            del mutant.fitness.values

        # Evaluate the individuals with an invalid fitness
        
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        
        fitnesses = list()
        for i in invalid_ind:
            n= toolbox.evaluate(i,FS,LF)
            fitnesses.append(n)
    
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # The population is entirely replaced by the offspring
        pop[:] = offspring

    return pop

"""To execute copy this"""
#main(3,3,0.5,0.3,[0,1,0,-1,0,0,0,-1,0],[1,0,0,0,-1,0,0,-1,0])
